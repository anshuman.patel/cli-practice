Drill 2
--------

Task-1 :- Download the contents of "Harry Potter and the Goblet of fire" using the command line from
-----------------------------------------------------------------------
wget https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt

Task-2 :- Print the first three lines in the book
-----------------------------------------------------------------------
head -n 3 H*.txt

Task-3 :- Print the last 10 lines in the book
-----------------------------------------------------------------------
tail -n 10 H*.txt

Task-4 :- How many times do the following words occur in the book?
-----------------------------------------------------------------------
grep -o -i "Ron" H*.txt | wc -l
grep -o -i "Harry" H*.txt | wc -l
grep -o -i "Hermione" H*.txt | wc -l
grep -o -i "Dumbledore" H*.txt | wc -l

Task-5 :- Print lines from 100 through 200 in the book
-----------------------------------------------------------------------
sed -n '100,200p' H*.txt

Task-6 :- How many unique words are present in the book?
-----------------------------------------------------------------------
grep -o -i '\b\w*' H*.txt | sort -u | wc -l

Task-7 :- List your browser's process ids (pid) and parent process ids(ppid)
-----------------------------------------------------------------------
pgrep firefox | ps -o pid,ppid

Task-8 :- Stop the browser application from the command line
-----------------------------------------------------------------------
pkill firefox

Task-9 :- List the top 3 processes by CPU usage.
-----------------------------------------------------------------------
ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head -n 4

Task-10 :- Install git, vim and nginx
-----------------------------------------------------------------------
sudo apt-get install git vim nginx

Task-11 :- Uninstall nginx
-----------------------------------------------------------------------
sudo apt-get remove nginx

Task-12 :- What's your local ip address?
-----------------------------------------------------------------------
hostname -I

Task-13 :- Find the ip address of google.com
-----------------------------------------------------------------------
dig +short google.com @resolver1.opendns.com